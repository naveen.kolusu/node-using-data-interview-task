var express = require('express');
var router = express.Router();
var request = require('request');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var session = require('express-session');
var url = "mongodb://myadmin:myadmin1@ds043047.mlab.com:43047/node1db";
// var url = "mongodb://localhost:27017/mydb";
/* GET home page. */
var mongoDb ;
MongoClient.connect(url, function(err, db) {
if (err) throw err;
console.log("Database created!");
var db = db.db('node1db');
mongoDb = db;
//db.close();
});
router.get('/', function(req, res, next) {
  MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
  var db = db.db('node1db');
  mongoDb.collection('mydata').find({}).toArray(function(err, data){
    if(err){
      return err;
    }
     //var x = JSON.stringify(data);
    //console.log(x);
    // data.forEach(function(doc) {
    //   console.log(doc.surname);
  //});


  console.log("req.session.login", req.session.login);
  if(req.session.login){
    var login = true;
    console.log("req.session.login", req.session.login);
    var editedData=[];
    for(var i=0; i<data.length; i++){
      var d= {};
      d= data[i];
      d["login"] = true;
      editedData.push(d);
    }
      var dataPost = editedData;
  }else{
    var login = false;
    console.log("no req.session");
    var dataPost = data;
  }

  console.log(editedData)
    res.render('display', {names : dataPost,customers : data.customers , login:login});
  })

});
 });

// insert new project
router.get('/insert', function(req, res){
  res.render("index");
})


router.post('/insert', function(req, res){
  var name = req.body.firstname;
  var surname = req.body.lastname;
  mongoDb.collection('mydata').insertOne({"name":name,"surname":surname},function(err,update){
    if(err){
      return err;
    }
    console.log('updated');
    res.redirect('/')
  })
})
// for customer
router.get('/customer/:id', function(req,res, next){
  var id = req.params.id;
  console.log("id", id);
  mongoDb.collection('mydata').findOne({"_id":new ObjectId(id)},function(err, result){
    if(err){
      console.log(err);
    }
    else {
      console.log("result",result);
    }
    res.render('customer', {"name" : result.name, "surname" : result.surname, "_id" : result._id});
  })
});
// for customer create
router.post('/customer/:id', function(req,res, next){
  var id = req.params.id;
  var name = req.body.firstname;
  var cname  = req.body.name;
  var phone = req.body.phone;
  var email = req.body.email;
  var obj = {};
var keyValue = {"name" : cname, "phone" : phone, "email":email};
var keyName = "customers." + cname;
obj[keyName] = keyValue;
mongoDb.collection("mydata").updateOne({"_id":new ObjectId(id)},{$set:obj}, function(err, result){
if(err){
console.log(err);
console.log("this error");
}
})
res.redirect("/");

});

// for a customer update
router.get('/updatecustomer/:id', function(req,res, next){
  var id = req.params.id;
  console.log("id", id);
  mongoDb.collection('mydata').findOne({"_id":new ObjectId(id)},function(err, result){
    if(err){
      console.log(err);
    }
    else {
      console.log("result",result);
    }
    res.render('update', {"name" : result.name, "customers" : result.customers,"email":result.email, "_id" : result._id});
  })
});
// for customer update fields
router.post('/customerupdate/:id', function(req,res, next){
  var id = req.params.id;
  var name = req.body.firstname;
  var cname  = req.body.name;
  var phone = req.body.phone;
  var email = req.body.email;
  var obj = {};
var keyValue = {"name" : cname, "phone" : phone, "email":email};
var keyName = "customers." + cname;
obj[keyName] = keyValue;
mongoDb.collection("mydata").updateOne({"_id":new ObjectId(id)},{$set:obj}, function(err, result){
if(err){
console.log(err);
console.log("this error");
}else{console.log("successfully updated");}
})

res.redirect("/");
 
});

// for project delete 
router.get('/customerdelete/:id', function(req,res, next){
  var id = req.params.id;

  mongoDb.collection('mydata').deleteOne({"_id":new ObjectId(id)},function(err, result){
    if(err){
      console.log(err);
    }
    else {
      console.log('deleted');
      console.log("result",result);
    }
    res.redirect("/")
  })
});


// for registration users
router.get('/signup', function(req, res){
  res.render("signup");
})

router.post('/signup', function(req, res){
  console.log("hello");
  var name = req.body.fname;
  var email = req.body.email;
  var password = req.body.pwd;
  var cpassword = req.body.cpwd;

  mongoDb.collection('users').insertOne({"name":name,"email":email,"password":password,"cpassword":cpassword},function(err,update){
    if(err){
      return err;
    }
    console.log('updated');
    console.log(update);
    res.redirect('/')
  })
})

// for user login
router.get('/login', function(req, res){
  res.render("login");
})

router.post('/login', function(req,res, next){
  var email = req.body.email;
  var password = req.body.pwd;
  mongoDb.collection('users').findOne({"email":email,"password":password},function(err, result){
    if(err){
      console.log(err);
      console.log("not a valid user")
    }
    else {
      console.log("result",result);
      if(result == null){
        console.log("is not a valid user");
        req.session.login = false;
        res.redirect("/")

      }else{
        req.session.login = true;
        console.log("req.session.login", req.session.login);

      }
    }
res.redirect("/")

  })

})

router.get('/logout', function(req, res){
  req.session.destroy();
  res.redirect("/")
})

// router.get('/update/:id', function(req,res, next){
//   var id = req.params.id;
//   console.log("id", id);
//   mongoDb.collection('mydata').findOne({"_id":new ObjectId(id)},function(err, result){
//     if(err){
//       console.log(err);
//     }
//     else {
//       console.log("result",result);
//     }
//     res.render('update', {"name" : result.name, "surname" : result.surname, "_id" : result._id});
//   })
// });
// router.post('/update/:id', function(req,res, next){
//   var id = req.params.id;
//   var name  = req.body.firstname;
//   var surname = req.body.lastname;
//   mongoDb.collection('mydata').updateOne({"_id":new ObjectId(id)},{$set:{"name": name, "surname" : surname}},function(err, result){
//     if(err){
//       console.log(err);
//     }
//     else {
//       console.log("result",result);
//     }
//     res.redirect("/")
//   })
// });
// router.get('/delete/:id', function(req,res, next){
//   var id = req.params.id;

//   mongoDb.collection('mydata').deleteOne({"_id":new ObjectId(id)},function(err, result){
//     if(err){
//       console.log(err);
//     }
//     else {
//       console.log('deleted');
//       console.log("result",result);
//     }
//     res.redirect("/")
//   })
// });

module.exports = router;
